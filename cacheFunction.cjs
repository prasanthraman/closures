
function cacheFunction(cb) {
    if(typeof(cb)!=='function'){
        return null //Not a function
    }
    const cache = {}
    return function cachedFunction() {
        let key = Array.from(arguments)
        if (Object.keys(cache).includes(key.toString())) {
            console.log('Result was fetched from Cache for Arguments: ' + key)
            return cache[key] // Result fetched from cache history
        }
        cache[key] = cb(...arguments) // Cb funtions gets executed by passing arguemnts with rest operator and stored as value in cache object
        return cache[key]
    }

}
module.exports = cacheFunction;
