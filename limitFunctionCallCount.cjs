function limitFunctionCallCount(cb, n) {
    if(typeof(cb)!=='function' || typeof(n) !='number'){
        return null // Incorrect arguements
    }
    let remains=n
    return function limit(){
        if(remains>0){
            remains-- // remains now gets the value of 'n' inside closure scope
            return cb()
        }else{
            return null
        }
    }
}
module.exports=limitFunctionCallCount;