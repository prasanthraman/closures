
function counterFactory() {
    let counter=0 // counter variable 
    const obj={ 
                increment(){
                    counter++ // counter variable accessed inside closure
                    return counter
                            },
                decrement(){
                    counter--
                    return counter
                }
                }
    return obj
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
}

module.exports=counterFactory
